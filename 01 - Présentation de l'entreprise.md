# L'IRCAD

![IRCAD France](images/ircad-france.jpg)

> There is no better way to learn

Le 20e siècle fut témoin de nombreuses innovations. Conséquence de la troisième révolution industrielle, également appelée révolution numérique, l'émergence de l'informatique ouvre de nouvelles opportunités et apporte de nombreuses possibilités inédites dans tous les domaines.

En 1991, le Professeur Jacques Marescaux assiste, à Cologne, à une conférence portant sur le sujet de la robotique. C'est lors de cette conférence que le professeur prend conscience des avancées de la robotique et de l'informatique. Ainsi, en 1992, une idée germe dans son esprit : comment mettre la robotique et la puissance informatique au service du chercheur, du médecin, du chirurgien et du radiologue pour les aider à améliorer leurs diagnostics et leurs soins ? C'est ainsi que commence l'histoire de l'Institut de Recherche contre les Cancers de l'Appareil Digestif. Deux orientations sont d'ores et déjà définies : la recherche de connaissances nouvelles et la transmission du savoir acquis.

---

## L'histoire de l'institut

Fondé en 1994, l'Institut de Recherche contre les Cancers de l'Appareil Digestif est un institut privé de recherche et de formation situé dans l'enceinte de l'Hôpital Civil de Strasbourg. L'institut se démarque de par son approche inédite du traitement du cancer, qui n'a alors rien à voir avec ce qui pouvait se faire à cette époque. Amener l'informatique au même rang que les sciences de l'infiniment petit, de la cellule et du génome, représente un réel défi.

En moins de 20 années, l'IRCAD devient une référence mondiale en matière de recherche et de formation à la chirurgie mini-invasive. Comment passe-t'on du statut de précurseur à celui de leader incontesté en si peu de temps ? Selon le Professeur Marescaux, cette notoriété s'est acquise avec la réalisation de premières mondiales.

* En 1996, l'IRCAD participe à la première reconstruction tridimensionnelle au monde du Visible Human. Démarré en 1989, le Visible Human Project visait à créer une base de données de photographies de sections du corps humain. En ce sens, deux cadavres, celui d'un homme ainsi que celui d'une femme, on été découpes en fines lamelles. C'est à partir des coupes de l'homme qu'ont été acquises les images scanner et IRM utilisées par les chercheurs de l'IRCAD afin de réaliser la modelisation de chaque organe du Visible Human Man.

* En 1997, les chirurgiens de l'institut réalisent la première chirurgie expérimentale assistée par ordinateur.

* En 1998, la première modélisation tridimensionnelle intégrale d'un corps humain est réalisée par les équipes de l'IRCAD à partir de l'image scanner d'un patient.

* En 2000, le premier site internet entièrement dédié à l'éducation en matière de chirurgie mini-invasive est lancé. Websurg voit le jour.

* En 2001, un partenariat entre des ingénieurs de France Telecom, d'Alcatel, de l'IRCAD et du Professeur Marescaux porte ses fruits : la première intervention chirurgicale a distance a lieu. Alors que cela paraissait impossible, les délais de communication ont été réduit et maîtrisé d'un bout à l'autre de manière à permettre au Professeur Marescaux d'opérer, depuis New York, une patiente située à Strasbourg. L'opération consistait en la réalisation d'une cholecystectomie d'une patiente du service de chirurgie A de l'Hôpital Civil de Strasbourg. L'opération, d'une durée de 45mn, a été une franche réussite, propulsant l'institut sur le devant de la scène des nouvelles technologies médicales. Aujourd'hui encore, l'IRCAD est fréquement présenté comme l'institut ayant mené l'Opération Lindbergh. Le nom de cette opération fait référence à l'aviateur Charles Lindbergh, pionner de l'aviation américaine, premier pilote à relier New York à Paris, en solitaire et sans escale, en 1927.
![Opération Lindbergh](images/lindbergh.jpg)

* En 2007, second coup de tonnerre dans le milieu médical, l'Opération Anubis est la première opération chirurgicale sans cicatrice apparente. Réalisée par la voie transvaginale, sans aucune incision cutanée, l'Opération Anubis est nouvelle en son genre. Les bénéfices d'une telle opération sont multiples : facilité d'accès à certains organes, absence de traumatisme physique et enfin l'avantage cosmétique.
La patiente, âgée de 30 ans, souffrait de calculs de la vésicule biliaire responsables de plusieurs crises douloureuses. Les suites post-opératoires ont été marquées par l'absence totale de douleur, mais, par prudence, l'équipe chirurgicale a préconisé une courte hospitalisation de 48h.
![Opération Anubis](images/anubis.jpg)

* En 2008, le concept de l'IRCAD s'importe a Taiwan et donne naissance à l'ASIA IRCAD.

* En 2011, c'est à Barretos, au Brésil, que l'IRCAD Latin America est créé.

* Pour répondre à aux besoins de son activité croissante, l'IRCAD inaugure un nouveau bâtiment : l'IRCAD 2.

---

## L'organisation

L'IRCAD est une association soumise au régime de droit local et donc régie par les articles 21 à 79-III du Code Civil Local entré en vigueur le 1er 1900. Possédant ainsi la pleine capacité juridique, l'IRCAD est en mesure d'exercer des actes habituels de commerce bien que son objectif ne soit pas lucratif.

Avec un bugdet dépassant les 13 millions d'euros, contre 200 000 à sa création, l'institut est majoritairement financé par le biais de dons d'origines privées, d'évènements (dîners de gala, ventes aux enchères, tournois de golf, etc.), de financements publiques et enfin par les différentes formations assurées par l'EITS tout au long de l'année.

### La structure

La structure interne de l'IRCAD est une structure mixte représentée par des structures hierarchiques et des structures fonctionnelles. Parfaitement adaptée aux entreprises de taille moyenne et aux domaines d'activité diversifiés, cette organisation favorise la communication entre les différents acteurs de l'entreprise et permet à l'IRCAD d'optimiser ses ressources humaines et de s'adapter aux différents projets en cours.
Dans le cas de l'IRCAD, le Line est représenté par les départements de recherche et de formation, tandis que le Staff est représenté par le Professeur Jacques Marescaux.

![Structure de l'IRCAD](images/organigramme.png)

### Les hommes

L'IRCAD compte aujourd'hui 50 employés permanents répartis dans différents corps de métiers : chirurgiens, chercheurs, ingénieurs, vétérinaires, techniciens, responsables d'administrations, hôtesses d'accueil, etc. Cet effectif aux compétences diverses et complémentaires est appuyé par un nombre non négligeable de stagiaires et d'apprentis.

### La culture

Depuis sa création, l'IRCAD a toujours clairement affiché sa culture de l'excellence. L'institut, pour enrichir cette culture, a toujours su s'entourer des meilleurs talents pour chaque thématique abordée ainsi que de nombreux experts de renom. C'est au moyen d'objectifs communs, ambitieux et valorisants que les efforts de chacun convergent en un point, la recherche de l'innovation et le maintien du niveau d'excellence.

Cette culture se retrouve dans tous les niveaux de l'institut. Ainsi, il sera attendu le meilleur même des stagiaires et apprentis qui peuvent compter sur leurs encadrants pour les aider à donner le meilleur d'eux-même.


---

## Les activités

### La recherche

Pierre angulaire de l'IRCAD, la recherche est encadrée par Jean-Marc Egly en sa qualité de président du conseil scientifique. Les différentes équipes de recherche de l'institut conjuguent leurs efforts pour prévenir le cancer, améliorer son diagnostic et son traitement. Aujourd'hui, plus de 1000 publications et 2000 communications témoignent de cette collaboration forte entre les équipes.

#### Informatique

Dans la recherche contre le cancer, la détection précoce et le traitement chirurgical des tumeurs permettent d’améliorer le taux de survie des patients. Les interventions chirurgicales restent toutefois des procédures complexes qui pourraient être significativement améliorées grâce au développement de la chirurgie assistée par ordinateur. Une telle amélioration peut se résumer en quatre étapes majeures :

- La première consiste à automatiser la modélisation 3D des patients à partir de leur imagerie médicale.
- La seconde consiste à utiliser cette modélisation avec un logiciel de planification et de simulation chirurgicale, offrant ainsi la possibilité de s’entraîner au geste chirurgical avant son exécution.
- La troisième étape consiste à recaler de manière intra-opératoire des données préopératoires sur la vue réelle du patient. Cette Réalité Augmentée fournit aux chirurgiens une vue en transparence de leur patient permettant de suivre les instruments et d’améliorer le ciblage des pathologies.
- La dernière étape consiste à robotiser la procédure en remplaçant le geste humain par un geste robotique qui peut être automatisé (équipe AVR d’iCube).

#### Robotique

L’équipe de recherche AVR (Automatique, Vision, Robotique) recherche, conçoit et évalue de nouvelles solutions pour la médecine et la chirurgie assistée par ordinateur. Son expertise s’étend de la conception mécatronique à la commande robotique et la vision par ordinateur.
L’activité de recherche aborde en particulier la commande par vision endoscopique et par retour imageur, la commande de dispositifs médicaux pour la compensation de mouvements physiologiques, la commande de systèmes à câbles, la téléopération, la supervision et la modélisation des gestes médicaux, la reconstruction 3D temps-réel par lumière structurée et la conception de dispositifs pour l’imagerie interventionnelle.

### L'enseignement

L'activité de recherche de l'IRCAD nourrit sa seconde activité : l'enseignement. En 2015, ce ne sont pas moins de 5000 chirurgiens de 116 nationalités qui sont formés à l'IRCAD, plus de 2000 à l'ASIA IRCAD et plus 2500 à l'IRCAD Latin America.

#### EITS

L'European Institute of TeleSurgery (EITS) est le centre de formation en chirurgie mini-invasice de l'IRCAD. Un pool de 800 experts internationaux, leaders d’opinion dans leur spécialité chirurgicale, encadrent l’ensemble des cours dispensés à l’IRCAD , dans les spécialités de chirurgie générale, digestive, perianale et transanale, colorectale, bariatrique et métabolique, hépatobiliaire et pancréatique, herniaire, urologique, endoscopie chirurgicale, chirurgie pédiatrique, gynécologique, thoracique, de neurochirurgie et d’arthroscopie.
Pour supporter cette importante activité de formation, l'institut dispose d'un laboratoire expérimental équipé de 17 tables opératoires. Cet équipement est complété par deux blocs expérimentaux consacrés aux formations de pilotage des robots Da Vinci.

#### WebSurg

Créée par le Professeur Jacques Marescaux et son équipe de l’EITS, WeBSurg est une Université Virtuelle spécialisée en chirurgie accessible partout dans le monde via Internet. L'objectif est d’apporter à la communauté chirurgicale, aux sociétés savantes, aux écoles médicales et aux industriels la première formation chirurgicale mondiale en ligne, de l’information sur les innovations chirurgicales, la possibilité de dialoguer avec des praticiens et des experts du monde entier ainsi qu'une diffusion toujours plus rapide à l’échelle mondiale de la connaissance chirurgicale pour offrir à la communauté médicale un accès immédiat aux dernières innovations et améliorer le confort et le bien-être des patients.

---

## Les investissements

### IHU

En janvier 2012, l’institut a contribué à la création de l’IHU Strasbourg (Institut Hospitalo-Universitaire) avec un budget de 200 millions d’euros. L’IRCAD fait partie des sept membres fondateurs du projet. Ce nouvel institut, dont la construction sera achevée en 2016, est une fondation de coopération scientifique faisant partie du plan d’investissements d’avenir créé par le gouvernement français. Le bâtiment sera relié à l’IRCAD et au Nouvel Hôpital Civil via des passerelles permettantes de créer un dynamisme entre les différentes constructions. L’IHU aura pour objectif de faire émerger une nouvelle discipline médicale hybride combinant les expertises de chirurgiens, gastro-entérologues et radiologues.

![Maquette de l'IHU](images/ihu.jpg)

### Visible patient

En juillet 2013, le Professeur Marescaux s’est associé au Professeur Luc Soler afin de créer Visible Patient. Cette jeune entreprise a pour objectif d’exploiter les travaux de recherche réalisés par l’IRCAD afin d’en tirer une exploitation commerciale. Ainsi, Visible Patient souhaite s’imposer comme une entreprise leader en matière de reconstructions d’organes 3D. Ces modèles 3D auront pour objectif, dans les années à venir, d’améliorer le diagnostic et d’augmenter la précision des opérations chirurgicales grâce à la réalité augmentée. La start-up est installée actuellement au sein du biocluster des haras.

### Les Haras

En 2009, l'IRCAD signe un bail emphytéotique de 52 ans les octoyant les droits de réhabilitation et d'exploitation des anciens Haras Nationaux de Strasbourg, construits au milieu du XVIIIe siècle et classés au titre des monuments historiques dès 1922. Les bâtiments accueillent, depuis 2014, un hôtel quatre étoiles, une brasserie ainsi que le Biocluster.
L'hôtel et la brasserie représente une source de financement supplémentaires et accueillent un grand nombre des chirurgiens au cours de leur formation à l'IRCAD.
Le Biocluster est une pépinière d'entreprise pouvant accueillir une vingtaine de jeunes entreprises et leur offrir un environnement favorable à leur développement.

![Photographie du Biocluster](images/biocluster.jpg)

### L'IRCAD 2

L'IRCAD, qui voit son activité de formation en chirurgie mini-invasive augmenter chaque année a décidé d’ouvrir une seconde plate-forme d’enseignement et de chirurgie expérimentale dans un nouveau bâtiment, l’IRCAD 2. L’Institut y  accueille depuis quelques mois deux géants mondiaux de l’industrie chirurgicale, Medtronic et Intuitive Surgical, sur les 4 niveaux que compte le bâtiment.

---

## Les partenaires

### ASIA IRCAD

En 2006, le président d'un groupe d'hôpitaux de Taiwan, le Professeur Min-Ho Huang, rencontre le Professeur Marescaux et lui expose son projet d'ouvrir un IRCAD à Taiwan. Il convainc le Professeur Marescaux très rapidement et, 13 mois plus tard, l'IRCAD Taiwan vit le jour en face du Chang Bing Show Chwan Memorial Hospital.

### IRCAD Latin America

Un richissime fermier brésilien, très catholique, contacte le Professeur Marescaux suite à une apparition de la Vierge lui demandant de dépenser sa fortune familliale au profit des pauvres du Brésil. C'est ainsi que s'est créé l'IRCAD Brasil, plus grand hôpital spécialisé dans la lutte contre le cancer au Brésil, totalement gratuit pour les plus démunis. Une flotte d'une vingtaine de bus circule dans toutes les régions brésiliennes afin de diagnostiquer les malades et de les ramener, ainsi que leur famille, à l'institut pour les y soigner.

### L'IHU

Les équipes de l'IRCAD et de l'IHU travaillent ensemble le cadre de la recherche et du développement.
Dès l'inauguration de son nouveau bâtiment, cet institut offrira en outre des formations adaptées aux étudiants et aux professionnels. Un suivi clinique des nouvelles technologies et procédures pour les patients, ainsi que leurs évaluations médico-économiques permettront également à ce nouvel institut de rester à la pointe du progrès.

### Karl Storz

Entreprise familliale fondée en 1945, la société Karl Storz est un leader mondial de la production et de la commercialisation d'endoscopes, d'instruments et d'appareils médicaux. Ce partenariat permet à l'IRCAD de jouir des innovations et du matériel de la société afin d'appuyer la recherche et l'enseignement.

### MedTronic

Afin de renforcer ses liens avec Covidien (devenu Medtronic Minimally Invasice Therapies depuis son acquisition par le groupe Medtronic), partenaire historique de l’institut depuis sa création en 1994, l’IRCAD a mis à la disposition de l’industriel 3 étages de l’IRCAD 2. Medtronic a de ce fait délocalisé à Strasbourg son centre européen de formation destiné aux équipes marketing et force de vente d’Elancourt (Yvelines). Près de 4 500 journées de formation se sont ainsi déroulées à l’IRCAD 2 entre mai 2014 et mai 2015, permettant aux collaborateurs de Medtronic d’enrichir leur formation au contact des professionnels de santé présents sur le campus de l’IRCAD.

### Intuitive Surgical

La société américaine Intuitive Surgical, leader mondial de la chirurgie mini-invasive assistée par robot, a choisi d’installer son plus gros centre de formation en Europe sur un étage de l’IRCAD 2, afin de donner l’opportunité aux nombreux chirurgiens en formation à l’Institut de tester les robots Da Vinci. Intuitive Surgical bénéficie de l’environnement académique de l’IRCAD pour former les chirurgiens aux nouvelles technologies de robotique chirurgicale : 6 robots Da Vinci, équipés de double-consoles et de simulateurs de chirurgie, se déploient sur les 370 m2 du tout nouveau laboratoire expérimental.
 
